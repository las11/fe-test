var instagramAppControllers = angular.module('instagramApp.controllers', []);

var AppController = instagramAppControllers.controller('AppController', function ($rootScope, $scope, $state, AuthenticationService, instagramApi,Analytics) {

    $scope.isLoggedIn = function(){

        if(AuthenticationService.isLoggedIn()){

            return true;

        }else{
            return false;
        }

    };

    //common variables
    $rootScope.authLink = AuthenticationService.getAuthLink();

    if($rootScope.globals.currentUser) {

        AuthenticationService.getRequestedBy(function(response){

            $scope.serviceMeta = response.meta;

            $rootScope.userRequests = response.data;

        });

    }

    //common method
    $scope.giveLike = function(mediaId){

        instagramApi.giveLike(mediaId);

    }

    $scope.isOwn = function(userId){

        if( $scope.isLoggedIn() ) {

            if( AuthenticationService.getCurrentUser().userId == userId ) {

                return true;

            }

        }else {

            return false;

        }

    }

});

var navController = instagramAppControllers.controller('navController', function($scope, AuthenticationService, $location){

    $scope.isLoggedIn = function(){

        if(AuthenticationService.isLoggedIn()){

            $scope.user = AuthenticationService.getCurrentUser();

            return true;

        }else{
            return false;
        }

    };

    $scope.signOut = function(){

        AuthenticationService.ClearCredentials();

        $location.path("/#");

    };

    $scope.refresh = function(){

        AuthenticationService.getUserSelf();

    };

});


var indexController = instagramAppControllers.controller('IndexController', function ($scope, instagramApi) {

    $scope.serviceMeta = {};

    $scope.feed = [];

    $scope.refreshFeed = function(nextMaxId){

        instagramApi.userSelfFeed(function(response){

            $scope.serviceMeta = response.meta;

            $scope.feed = $scope.feed.concat(response.data);

            $scope.nextIterator = response.pagination.next_max_id;

        }, nextMaxId);

    };

    $scope.loadMore = function(){

        $scope.refreshFeed($scope.nextIterator);

    };

});

var loginController = instagramAppControllers.controller('loginController', function ($scope, $stateParams, AuthenticationService, instagramApi, $location) {

    $scope.$on('$viewContentLoaded', function () {

        $scope.accessToken = $stateParams.accessToken;

        AuthenticationService.setAuth($scope.accessToken);

        AuthenticationService.getUserSelf(function (response) {

            $scope.serviceMeta = response.meta;

            $location.path("/#");

        });

    });

});


var userController = instagramAppControllers.controller('userController', function ($scope, instagramApi, $stateParams) {

    $scope.setLayout = function (layout) {
        $scope.layout = layout;
        $scope.nextIterator = false;
    };

    $scope.isLayout = function (layout) {
        return $scope.layout == layout;
    };

    $scope.userId = $stateParams.userId;

    $scope.userName = $stateParams.userName;

    $scope.images = [];

    $scope.follows = [];

    $scope.followedBy = [];

    $scope.serviceMeta = {};

    $scope.usedNextIterators = [];

    $scope.iterators = [];

    $scope.getUserProfile = function () {

        instagramApi.getUser($scope.userId, function (response) {

            $scope.serviceMeta = response.meta;

            if ($scope.serviceMeta.code == 200) {

                $scope.user = response.data;

                $scope.getRecentMedia();

                if( $scope.isLoggedIn() && !$scope.isOwn() ){

                    instagramApi.relationship($scope.user.id, 'relationship', function(data) {

                        $scope.relationshipData = {};

                        if(data.data.outgoing_status == "follows"){

                            $scope.relationshipData.following = true;

                        }

                    })

                }

            }

        });

    };

    $scope.getRecentMedia = function (nextIterator) {

        //setting layout
        $scope.setLayout('getRecentMedia');

        instagramApi.getRecentMedia($scope.userId, function (response) {

            $scope.serviceMeta = response.meta;

            $scope.images = $scope.images.concat(response.data);

            $scope.nextIterator = response.pagination.next_max_id;

        }, nextIterator);

    };

    $scope.getFollows = function(nextIterator){

        //setting layout
        $scope.setLayout('getFollows');

        instagramApi.getFollows($scope.userId, function(response){

            $scope.serviceMeta = response.meta;

            $scope.follows = $scope.follows.concat(response.data);

            $scope.nextIterator = response.pagination.next_cursor;

        }, nextIterator);

    };

    $scope.getFollowedBy = function(nextIterator){

        //setting layout
        $scope.setLayout('getFollowedBy');

        instagramApi.getFollowedBy($scope.userId, function(response){

            $scope.serviceMeta = response.meta;

            $scope.followedBy = $scope.followedBy.concat(response.data);

            $scope.nextIterator = response.pagination.next_cursor;

        }, nextIterator);

    };

    $scope.relationship = function(userId, action){

        instagramApi.relationship(userId, action);

    };


    $scope.loadMore = function(){

        $scope.usedNextIterators.push($scope.nextIterator);

        eval("$scope."+$scope.layout+"('"+$scope.nextIterator+"')");

    };

});


var mediaController = instagramAppControllers.controller('mediaController', function ($scope, instagramApi, $stateParams) {

    $scope.mediaId = $stateParams.mediaId;

    $scope.mediaType = "";

    $scope.serviceMeta = {};

    $scope.getMedia = function(){

        instagramApi.getMedia($scope.mediaId, function (response) {

            $scope.serviceMeta = response.meta;

            $scope.media = response.data;

            if($scope.media.location) {

                $scope.map = {

                center:{latitude: $scope.media.location.latitude, longitude: $scope.media.location.longitude },

                zoom: 19,

                options : {

                        mapTypeId : 'satellite'

                    }

                };

                $scope.marker = {
                    id: 0,
                    coords: {
                        latitude: $scope.media.location.latitude,
                        longitude: $scope.media.location.longitude
                    }
                };

            }

            instagramApi.getComments($scope.mediaId, function(response){

                $scope.media.comments.data = response.data;

            });

        });

    };

});

