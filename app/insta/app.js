var apps = angular.module('instagramApp', ['instagramApp.controllers', 'instagramService', 'ui.bootstrap', 'ui.router',
    'Authentication', 'directives', 'ngCookies', 'ui.unique', 'uiGmapgoogle-maps','angular-google-analytics']);

apps.constant('instagramApiConfig', {
        apiUrl: 'https://api.instagram.com/v1/',
        clientId: '11cdab9c3cbd43c188fbbefb2e519aa5',
        callback: 'http://127.0.0.1/las11/callback.html'
    }
);


apps.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

    // Redirect any unresolved url
    $urlRouterProvider.otherwise("/");

    $stateProvider

        .state('index', {
            url: "/index2.html",
            templateUrl: "/index2.html",
            data: {pageTitle: 'Home', pageSubTitle: ''},
            controller: "IndexController"
        })

        .state("user", {
            url: "/user/:userId/:userName",
            templateUrl: "/index2.html",
            data: {pageTitle: 'User', pageSubTitle: ''},
            controller: "userController"
        })

        .state("media", {
            url: "/media/:mediaId/:mediaType",
            templateUrl: "views/media.html",
            data: {pageTitle: 'Media', pageSubTitle: ''},
            controller: "mediaController"
        })

        .state("login", {
            url: "/login/:accessToken",
            templateUrl: "/index.html",
            data: {pageTitle: 'Login', pageSubTitle: ''},
            controller: "loginController"
        })

}]);

apps.run(["$rootScope", 'AuthenticationService', 'instagramApi', 'instagramApiConfig', '$state', function ($rootScope, AuthenticationService, instagramApi, instagramApiConfig, $state) {

    instagramApi.setCredentials(instagramApiConfig);

    AuthenticationService.start(instagramApi);

    $rootScope.$state = $state;

    $rootScope.errorCodes = instagramApi.errorCodes;

}]);